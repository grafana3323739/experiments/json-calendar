# Example usage of JSON plugin and calendar for grafana 

This project set up to demonstrate a calendar in grafana dashboard
The example data comes from grafana plugins, which also include a date for last update, which is
displayed in the calendar


## Requirements

```bash
# let containers run in background
docker compose up -d 
```

## 🚀


## Cleanup

```bash
docker-compose down
```

